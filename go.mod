module bnl.gov/xvcserver

go 1.17

replace bnl.gov/goxvcdriver => ./xvcdriver

require bnl.gov/goxvcdriver v0.0.0-00010101000000-000000000000

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
