package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"log"
	"net"
)

func ToULEB128(value uint64) (result []byte) {

	var len int
	if value == 0 {
		result = []byte{0}
		return
	}

	for value > 0 {
		result = append(result, 0)
		result[len] = byte(value & 0x7F)
		value >>= 7
		if value != 0 {
			result[len] |= 0x80
		}
		len++
	}

	log.Printf("%v encoded as %v \n", value, result)
	return

}

type XVCClient struct {
	addr string
	conn net.Conn
	io   *bufio.ReadWriter
}

func MakeMrdMsg(address, nbytes int) []byte {
	output := make([]byte, 0)
	output = append(output, []byte("mrd:")...)
	output = append(output, ToULEB128(0)...)
	output = append(output, ToULEB128(uint64(address))...)
	output = append(output, ToULEB128(uint64(nbytes))...)
	return output
}

func MakeMwrMsg(address, nbytes int, data []byte) []byte {
	output := make([]byte, 0)
	output = append(output, []byte("mwr:")...)
	output = append(output, ToULEB128(0)...)
	output = append(output, ToULEB128(uint64(address))...)
	output = append(output, ToULEB128(uint64(nbytes))...)
	output = append(output, data...)
	return output
}

func MakeShiftMsg(nbits int, tms, tdi []byte) []byte {
	output := make([]byte, 0)
	output = append(output, []byte("shift:")...)
	buf := make([]byte, 8)
	binary.LittleEndian.PutUint64(buf, uint64(nbits))
	output = append(output, buf...)
	output = append(output, tms...)
	output = append(output, tdi...)
	return output
}

func MakeSettckMsg(period uint64) []byte {
	output := make([]byte, 0)
	output = append(output, []byte("settck:")...)
	buf := make([]byte, 8)
	binary.BigEndian.PutUint64(buf, period)
	output = append(output, buf...)
	return output
}

func (client *XVCClient) Init(address string) {
	var err error
	client.addr = address
	client.conn, err = net.Dial("tcp", address)
	if err != nil {
		log.Fatal(err)
	}
	client.io = bufio.NewReadWriter(bufio.NewReader(client.conn), bufio.NewWriter(client.conn))
}

func (client *XVCClient) ProcessCommand(cmd []byte) {

	// reader := bufio.NewReader(os.Stdin)
	// fmt.Print("enter command: ")
	// cmd, _ := reader.ReadString('\n')
	// fmt.Printf("Received %v \n", cmd)

	_, err1 := client.io.Write([]byte(cmd))
	client.io.Flush()
	if err1 != nil {
		log.Fatal(err1)
	}
	fmt.Print("Waiting for answer\n")
	answer := make([]byte, 4096)
	n, err2 := client.io.Read(answer)
	if err2 != nil {
		log.Fatal(err2)
	}
	fmt.Printf("Answer : %v \n", (answer[0:n]))

}

func main() {

	client := XVCClient{}
	client.Init("localhost:1234")

	client.ProcessCommand(MakeSettckMsg(1234))
	client.ProcessCommand(MakeMrdMsg(0x1234, 4096))
	client.ProcessCommand(MakeMwrMsg(0x1234, 4, []byte{0x1, 0x2, 0x3, 0x4}))
	client.ProcessCommand(MakeShiftMsg(32, []byte{0x1, 0x2, 0x3, 0x4}, []byte{0x1, 0x2, 0x3, 0x4}))

}
