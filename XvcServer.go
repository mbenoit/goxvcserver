package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"bnl.gov/goxvcdriver"
)

const maxbytes uint64 = 4096

type XVCServer struct {
	ipaddr     string
	debug_addr uint64
	server     net.Listener
	driver     *goxvcdriver.XVCDriver
}

func (server *XVCServer) Init(address string, debug_addr uint64) {
	var err error
	server.ipaddr = address
	server.debug_addr = debug_addr
	server.server, err = net.Listen("tcp", server.ipaddr)
	if err != nil {
		log.Fatal(err)
	}
}

func (server *XVCServer) Run() {

	server.driver = &goxvcdriver.XVCDriver{}
	server.driver.Init(server.debug_addr, maxbytes)
	for {
		//Wait for a connection
		conn, err := server.server.Accept()

		if err != nil {
			log.Println("Failed to accept conn.", err)
			continue
		}

		fmt.Printf("Accepted connection and formed iobuf RW \n")
		for {

			//Read command from client
			cmd := make([]byte, maxbytes)
			n, err := conn.Read(cmd)
			if err != nil {
				//Dump this connection and go back to accepting of error
				log.Println(err)
				conn.Close()
				break
			}

			//Ask driver to process command and collect answer
			answer, err_rd := server.driver.ProcessCommand(cmd[0:n])

			if err_rd != nil {
				//Dump this connection and go back to accepting of error
				log.Println(err_rd)
				conn.Close()
				break
			}

			//send back answer to client
			_, err_wr := conn.Write(answer)
			//writer.Flush()
			log.Println("Sending answer : ", answer)
			if err_wr != nil {
				//Dump this connection and go back to accepting of error
				log.Println(err_wr)
				conn.Close()
				break
			}
		}
	}
}

func main() {
	address := flag.Uint64("a", 0x0, "base address of the debug hub")
	flag.Parse()

	server := XVCServer{}
	server.Init("localhost:1234", *address)
	server.Run()

}
