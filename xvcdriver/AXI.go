//go:build linux || darwin
// +build linux darwin

package goxvcdriver

import (
	"errors"
	"log"
	"os"
	"sync/atomic"
	"unsafe"

	"golang.org/x/sys/unix"

	mmap "github.com/codehardt/mmap-go"
)

const (
	AXIR int = iota
	AXIW
	AXIRW
)

// func bufftoarray(buff []byte) []uint32 {
// 	length := len(buff)
// 	array := make([]uint32, length/4)
// 	for i := 0; i < length; i += 4 {
// 		array[i/4] = (binary.BigEndian.Uint32(buff[i : i+4]))
// 	}
// 	return array
// }

// func arraytobuff(x []uint32) []byte {
// 	buf := make([]byte, len(x)*4)
// 	for i, v := range x {
// 		binary.BigEndian.PutUint32(buf[4*i:4*i+4], uint32(v))
// 	}
// 	return buf
// }

type axilitecontroller interface {
	BookRegisters(registers map[string][]int64)
	Read(name string) uint32
	Write(name string, value uint32)
}

type AXIController struct {
	axilitecontroller
	memmaps map[int64]mmap.MMap
	regmap  map[string][]int64
}

func (a *AXIController) BookRegisters(registers map[string][]int64) {

	if a.regmap == nil {
		a.regmap = make(map[string][]int64)
	}
	if a.memmaps == nil {
		a.memmaps = make(map[int64]mmap.MMap)
	}
	i := 0
	var err error
	var f *os.File
	for key, reg := range registers {
		//Check if map exist
		if a.memmaps[reg[0]] == nil {
			f, err = os.Open("/dev/mem")
			if err != nil {
				log.Fatal(err)
			}
			a.memmaps[reg[0]], err = mmap.MapRegion(f, 4096, mmap.RDWR, unix.MAP_SHARED|unix.MAP_FIXED, (reg[0]))
			if err != nil {
				log.Fatal(err)
			}
			f.Close()
		}
		a.regmap[key] = reg
		i++
	}

}

func (a *AXIController) Read(name string) uint32 {
	reg := a.regmap[name]
	if (int(reg[2]) == AXIR) || (int(reg[2]) == AXIRW) {
		return atomic.LoadUint32((*uint32)(unsafe.Pointer(&(a.memmaps[reg[0]])[4*reg[1]])))
	} else {
		log.Fatal("this register cannot be read", reg)
		return 0
	}
}

func (a *AXIController) Write(name string, value uint32) {
	reg := a.regmap[name]
	if (int(reg[2]) == AXIW) || (int(reg[2]) == AXIRW) {
		atomic.StoreUint32((*uint32)(unsafe.Pointer(&(a.memmaps[reg[0]])[4*reg[1]])), value)
	} else {
		log.Fatal("this register cannot be written", reg)
	}
}

type axistreamcontroller interface {
	BookPage(base, offset, length int64)
	Read(base, offset, length int64) *[]uint32
	Write(addr int64, values []uint32) error
}

type AXIStreamController struct {
	axistreamcontroller
	mem mmap.MMap
}

func (a *AXIStreamController) BookPage(base, length uint64) {
	var err error
	var f *os.File

	f, err = os.Open("/dev/zero")
	if err != nil {
		log.Fatal(err)
	}

	a.mem, err = mmap.MapRegion(f, int(length*4096), mmap.RDWR, unix.MAP_SHARED|unix.MAP_FIXED, int64(base))
	f.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func (a *AXIStreamController) Read(base, offset, length uint64) *[]uint32 {

	values := make([]uint32, length)
	for e := offset; e < offset+length; e++ {
		values[e-offset] = atomic.LoadUint32((*uint32)(unsafe.Pointer(&a.mem[e*4])))
	}
	return &values

}

func (a *AXIStreamController) Write(base, offset uint64, values []uint32) error {
	if len(values) > (len(a.mem[base+4*offset:]) / 4) {
		log.Fatal("array too long for memory")
		return errors.New("array too long for memory")
	}

	for i, v := range values {
		pos := base + 4*offset + uint64(i*4)
		atomic.StoreUint32((*uint32)(unsafe.Pointer(&a.mem[pos])), v)
	}
	return nil
}
