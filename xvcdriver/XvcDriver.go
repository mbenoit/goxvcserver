package goxvcdriver

import (
	"encoding/binary"
	"fmt"
	"log"
	"strings"
)

const DEFAULT_HUB_ADDR uint64 = 0xA4000000
const DEFAULT_HUB_SIZE uint64 = 0x200000

type XVCDriver struct {
	baseaddr uint64
	maxbytes uint64
	mem      AXIStreamController
}

func GetULEB128(buffer *[]byte, start uint64) (value, len uint64) {

	var i uint64
	i = 0
	n := byte(0)
	len = start
	for {
		n = (*buffer)[len]
		len++
		value |= uint64((n & 0x7F)) << i
		if n&0x80 == 0 {
			break
		}
		i += 7
	}

	return

}

func Uint32ToBytes(data []uint32) []byte {

	output := make([]byte, 0)

	for _, value := range data {
		bytes := make([]byte, 4)
		binary.BigEndian.PutUint32(bytes, value)
		output = append(output, bytes...)
	}

	return output
}

func BytesToUint32(data []byte) []uint32 {

	output := make([]uint32, 0)

	datalen := len(data)
	nuint := (datalen - (datalen % 4)) / 4
	if datalen%4 != 0 {
		nuint += 1
		for i := 0; i < datalen%4; i++ {
			data = append(data, 0x00)
		}
	}
	for i := 0; i < nuint; i++ {
		value := binary.BigEndian.Uint32(data[4*i : 4*i+4])
		output = append(output, value)
	}

	return output
}

func (driver *XVCDriver) Init(addr, maxbytes uint64) {
	driver.baseaddr = addr
	driver.maxbytes = maxbytes
	driver.mem = AXIStreamController{}
	driver.mem.BookPage(driver.baseaddr, DEFAULT_HUB_SIZE)
}

func (driver *XVCDriver) ProcessCommand(cmd []byte) (answer []byte, err error) {

	cmd_elements := strings.Split(string(cmd), ":")
	answerstr := ""
	switch cmd_elements[0] {
	case "getinfo":
		log.Println("Received GETINFO")
		answerstr += string(driver.getinfo(driver.maxbytes))
	case "mrd":
		buffer := []byte(cmd_elements[1])
		flags, len := GetULEB128(&buffer, 0)
		addr, len2 := GetULEB128(&buffer, len)
		nbytes, _ := GetULEB128(&buffer, len2)
		data, status := driver.mrd(flags, addr, nbytes)
		log.Printf("Received MDR : flags = %v, addr = %v, nbytes = %v", flags, addr, nbytes)
		answerstr += string(append(data, status))
	case "mwr":
		buffer := []byte(cmd_elements[1])
		flags, len := GetULEB128(&buffer, 0)
		addr, len2 := GetULEB128(&buffer, len)
		nbytes, len3 := GetULEB128(&buffer, len2)
		log.Printf("Received MWR : flags = %v, addr = %v, nbytes = %v, data: %v", flags, addr, nbytes, buffer[len3:])
		status := driver.mwr(flags, addr, nbytes, buffer[len3:])
		answerstr += string(status)
	case "settck":
		period := binary.BigEndian.Uint64([]byte(cmd_elements[1]))
		log.Printf("Received SETTCK with period %v \n", period)
		answerstr += string(driver.settck(period))
	case "shift":
		nbits := binary.LittleEndian.Uint64([]byte(cmd_elements[1][0:8]))
		nbytes := uint64(0)
		if nbits%8 == 0 {
			nbytes = nbits / 8
		} else {
			nbytes = (nbits-(nbits%8))/8 + 1
		}
		tms := []byte(cmd_elements[1][8 : 8+nbytes])
		tdi := []byte(cmd_elements[1][8+nbytes : 8+2*nbytes])
		log.Printf("Received SHIFT for nbits %v, tms = %v tdi = %v \n", nbits, tms, tdi)
		answerstr += string(driver.shift(3, tms, tdi))
	default:

	}

	answer = []byte(answerstr)

	return
}

func (driver *XVCDriver) mwr(flags, addr, nbytes uint64, data []byte) (status byte) {
	log.Printf("Writing %v bytes to address %x : %v", nbytes, addr, data)
	driver.mem.Write(addr, 0, BytesToUint32(data))
	status = 1
	return
}

func (driver *XVCDriver) mrd(flags, addr, nbytes uint64) (data []byte, status byte) {
	log.Printf("Reading %v bytes from address %x", nbytes, addr)
	uintdata := driver.mem.Read(addr, 0, nbytes/4)
	data = Uint32ToBytes(*uintdata)
	status = 0x1
	return
}

func (driver *XVCDriver) shift(nbytes uint64, tms, tdi []byte) (tdo []byte) {
	log.Printf("Shifting %v %v %v", tms, tdi, nbytes)
	tdo = []byte{0xDD, 0xFF, 0xAA}
	return
}

func (driver *XVCDriver) settck(period uint64) []byte {
	log.Printf("Setting TCK period to %v", period)
	retval := make([]byte, 16)
	binary.BigEndian.PutUint64(retval, period)
	return retval
}

func (driver *XVCDriver) getinfo(maxbytes uint64) []byte {

	answer := fmt.Sprintf("xvcServer_v1.1:%v\n", maxbytes)
	return []byte(answer)
}
